//
//  ViewController.m
//  FGModal
//
//  Created by fabio on 16/03/15.
//  Copyright (c) 2015 fabio. All rights reserved.
//

#import "ViewController.h"
#import "secondViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.button.clipsToBounds=YES;
    self.button.layer.cornerRadius=8;

}
- (IBAction)openPopup:(id)sender {

    secondViewController *second = [self.storyboard instantiateViewControllerWithIdentifier:@"secondViewController"];
    
    fgModal = [[FGModal alloc]init];
    [fgModal PresentPopupViewController:second inView:self.view withAlpha:0.7 centered:YES shadow:YES animate:YES dismissOnTouch:YES];
    
}


#pragma mark delegate methods

-(void)StartingPresentingPopup{
    NSLog(@"start!");
}

-(void)FinishPresentingPopup{
    NSLog(@"Finished :-(");
}



@end
