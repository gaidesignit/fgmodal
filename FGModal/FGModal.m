//
//  FGModal.m
//  FGModal
//
//  Created by fabio on 16/03/15.
//  Copyright (c) 2015 fabio. All rights reserved.
//

#import "FGModal.h"

@implementation FGModal

-(void)TapAction:(id)sender{
    
    UIView *view = [(UIGestureRecognizer *)sender view];
    [UIView animateWithDuration:0.3
                     animations:^{
                         
                         view.alpha=0;
                         
                     } completion:^(BOOL finished){
                         
                         [view removeFromSuperview];
                     }];
    
     [self.delegate FinishPresentingPopup];
    
}


-(void)DismissPopupViewController{
    
    UIResponder* nextResponder = [self.hostingView nextResponder];
    if ([nextResponder isKindOfClass:[UITableViewController class]])
    {
        UITableViewController *TableController = (UITableViewController*)nextResponder;
        TableController.tableView.scrollEnabled = YES;
    }
    
    [UIView animateWithDuration:0.3
                     animations:^{
                         
                         
                         self.UpViewController.view.alpha=0;
                         
                     } completion:^(BOOL finished){
                         
                         [self.UpViewController.view removeFromSuperview];
                     }];
    
     [self.delegate FinishPresentingPopup];
    
    
}

-(void)PresentPopupViewController:(UIViewController *)Controller inView:(UIView *)hostingView withAlpha:(CGFloat)alpha centered:(BOOL)centered shadow:(BOOL)shadowed animate:(BOOL)animated dismissOnTouch:(BOOL)shouldDismissOnTouch{
    
    [self PresentPopupViewController:Controller inView:hostingView withAlpha:alpha centered:centered shadow:shadowed animate:animated dismissOnTouch:shouldDismissOnTouch Completition:nil];
}

-(void)PresentPopupViewController:(UIViewController *)Controller inView:(UIView *)hostingView withAlpha:(CGFloat)alpha centered:(BOOL)centered shadow:(BOOL)shadowed animate:(BOOL)animated dismissOnTouch:(BOOL)shouldDismissOnTouch Completition:(void (^)(BOOL finished))completionBlock{
    
    [self.delegate StartingPresentingPopup];
    self.UpViewController = Controller;
    self.hostingView = hostingView;
    
    UIResponder* nextResponder = [hostingView nextResponder];
    if ([nextResponder isKindOfClass:[UITableViewController class]])
    {
        UITableViewController *TableController = (UITableViewController*)nextResponder;
        TableController.tableView.scrollEnabled = NO;
    }
    
    
    Controller.view.backgroundColor = [UIColor colorWithWhite:0.0 alpha:alpha];
    Controller.view.alpha=0;
    [hostingView addSubview:Controller.view];
    
    if (shouldDismissOnTouch) {
        
        UITapGestureRecognizer *Tap =
        [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(TapAction:)];
        [Tap setNumberOfTapsRequired:1];
        [Tap setNumberOfTouchesRequired:1];
        [Controller.view addGestureRecognizer:Tap];
    }
    
    
    
    for (UIView *view in Controller.view.subviews) {
        
        if (centered) {
            
            view.center =hostingView.center;
        }
        
        view.transform = CGAffineTransformMakeScale(.65, .65);
        view.opaque=YES;
        view.alpha=0;
        view.layer.cornerRadius=4;
        
        
        if (shadowed) {
            
            UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:view.bounds];
            view.layer.masksToBounds = NO;
            view.layer.shadowColor = [UIColor blackColor].CGColor;
            view.layer.shadowOffset = CGSizeMake(5.0f, -5.0f);
            view.layer.shadowOpacity = 0.3f;
            view.layer.shadowPath = shadowPath.CGPath;
        }
        
        if (animated) {
            
            [UIView animateWithDuration:.5 delay:0.0
                 usingSpringWithDamping:5.5 initialSpringVelocity:30.0f
                                options:0 animations:^{
                                    
                                    view.transform = CGAffineTransformMakeScale(1.0, 1.0);
                                    view.alpha=1;
                                    Controller.view.alpha=1;
                                    
                                } completion:nil];
            
            if (completionBlock) completionBlock(YES);
            
        }else{
            
            view.transform = CGAffineTransformMakeScale(1.0, 1.0);
            view.alpha=1;
            Controller.view.alpha=1;
            
            if (completionBlock) completionBlock(YES);
        }
        
        
        
        
        //block(view);
        
        
    }
    
    
}
@end
