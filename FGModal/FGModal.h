//
//  FGModal.h
//  FGModal
//
//  Created by fabio on 16/03/15.
//  Copyright (c) 2015 fabio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@class FGModal;
@protocol FGDelegate <NSObject>

-(void)StartingPresentingPopup;
-(void)FinishPresentingPopup;

@end

@interface FGModal : NSObject

@property UIViewController *UpViewController;
@property UIView *hostingView;

-(void)PresentPopupViewController:(UIViewController *)Controller inView:(UIView *)hostingView withAlpha:(CGFloat)alpha centered:(BOOL)centered shadow:(BOOL)shadowed animate:(BOOL)animated dismissOnTouch:(BOOL)shouldDismissOnTouch  Completition:(void (^)(BOOL finished))completionBlock;
-(void)PresentPopupViewController:(UIViewController *)Controller inView:(UIView *)hostingView withAlpha:(CGFloat)alpha centered:(BOOL)centered shadow:(BOOL)shadowed animate:(BOOL)animated dismissOnTouch:(BOOL)shouldDismissOnTouch;
-(void)DismissPopupViewController;

@property (nonatomic, assign) id <FGDelegate>delegate;

@end
