//
//  ViewController.h
//  FGModal
//
//  Created by fabio on 16/03/15.
//  Copyright (c) 2015 fabio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FGModal.h"

@interface ViewController : UIViewController<FGDelegate>{
    FGModal *fgModal;
}
@property (weak, nonatomic) IBOutlet UIButton *button;
@end

